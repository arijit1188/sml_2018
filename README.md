## sml_2018 ##

## Stochastic Methods and Lab, Fall 2018, Jacobs University ##

See the [course website](http://math.jacobs-university.de/petrat/teaching/2018_fall_stochastic_methods/) for more information.

Relevant Links:

* For an introduction to using and properly setting up git, see [Introduction to git for academics](https://bitbucket.org/marcel_oliver/git_for_academics/)

* For an introduction to Scientific Python, please read this [Introduction](http://math.jacobs-university.de/oliver/teaching/scipy-intro/scipy-intro.pdf), written by Marcel Oliver [html link](http://math.jacobs-university.de/oliver/teaching/scipy-intro/scipy-intro/index.html).
